#ifndef _STRING_H
# include <string.h>
#endif

#ifndef _STDLIB_H
# include <stdlib.h>
#endif

#ifndef _STDIO_H
# include <stdio.h>
#endif

#ifndef _UTILITY_H
# include "../utility.h"
#endif

#ifndef _BENCODE_H
# define _BENCODE_H
#endif

/**
 * bstring_free() free the memory occupied by the encoded string and set the \
 * string to NULL
 *
 * @param bstr pointer to bencoded string
 */
void bstring_free(char **bstr) {
	free(*bstr);
	*bstr = NULL;
}

/**
 * @function bencstr
 * @param dest pointer to the destination string
 * @param src, pointer to the string that has to be encoded
 * @return an integer which value is the bytes' number of the dest string (non
 * counting the terminating character
 *
 * FIX: insert escaping character for nick starting with a number!
 */
int bencstr(char **dest, const char *src)
{
	int len, plen;	//respectively the length of the string and the prefix

	if (!src) return -1;

	len = strlen(src);
	plen = ddigit(len + 1);
	*dest = (char *)malloc(plen + len + 2); //to include terminating and escape character
	sprintf(*dest, "%d\\", len + 1);

	strncpy(&(*dest)[plen + 1], src, len);
	(*dest)[plen + len + 1] = '\0';
	return plen + len + 1;
}

/**
 * @function bdecstr
 * @param dest pointer to dest string
 * @param src pointer to src string
 * @return the processed byte's number.
 *
 * FIX: insert escaping character for nick starting with a number!
 */
int bdecstr(char **dest, const char *src)
{
	int slen, startind = 0;

	if (!src) return 0;

	while(src[startind] >= '0' && src[startind] <= '9')
		startind++;

	sscanf(src, "%d", &slen);
	//removes escaping character from count
	slen--;

	*dest = (char *)malloc(slen*(sizeof(char)) + 1);
	if (!*dest) {
		perror("in bdecstr() during malloc(): ");
		exit(1);
	}

	strncpy(*dest, &src[startind + 1], slen);
	(*dest)[slen] = '\0';
	return slen + startind + 1;
}

/**
 * @function create_benclist Create a bencoded list of strings
 * @param dest pointer to pointer to the destination encoded list
 * @param src an array of string
 * @param len the length of src
 * @return the dests' bytes number
 *
 */
int create_benclist(char **dest, const char **src, const int len)
{
	int dim, i, slen;
	char **tmp;
	dim = 0;

	tmp = (char **)malloc(len*sizeof(char *));

	//first calculating dest dimension...
	for (i = 0; i < len; i++) {
		slen = strlen(src[i]);
		dim += ddigit(slen) + slen + 1; //for every string: strlen + strlen's digits and 1 escape
	}

	//...allocating...
	*dest = (char *)malloc((dim + 1)*sizeof(char));
	(*dest)[0] = '\0';

	//... and copying
	for (i = 0; i < len; i++) {
		bencstr(&tmp[i], src[i]);
		strcat(*dest, tmp[i]); //including terminating char

		//free tmp's strings space
		bstring_free(&tmp[i]);
	}

	//free tmp's space
	free(tmp);
	tmp = NULL;

	return strlen(*dest);
}

/**
 * @function extract_benclist create an array of string from a b-encoded \
 * string's list
 * @param dest destination array for strings.It has to be large enaugh to \
 * contain all the strings.
 * @param encoded the bencoded string
 */
void extract_benclist(char **dest, const char *encoded)
{
	int enclen, count, i;

	enclen = strlen(encoded);
	count = 0;
	i = 0;

	while (count < enclen) {
		count += bdecstr(&dest[i], &encoded[count]);
		i++;
	}
}
