#ifndef _TCOMMON_H
# include "common.h"
#endif

#ifndef _BENCODE_H
# include "bencode.h"
#endif

#ifndef _TRISMESSAGE_H

/*
 * Receive a tris message and encapsulate it into a struct trismsg
 * return the same value of the recv(). recv_tmsg returns -1 even if the msgcode
 * is not recognized.
 */
int recv_tmsg(int sockfd, struct trismsg * msg)
{
	int res;
	char msg_hdr[5];
	char *tmp;
	int payload_len;

	res = recv(sockfd, msg_hdr, sizeof(msg_hdr), MSG_WAITALL);
	if (res == -1) {
#ifdef DEBUG
		printf("DBG: %d < %d\n", res, sizeof(msg_hdr));
#endif
		perror("recv() while receiving message header: ");
		exit(1);
	}
#ifdef DEBUG
	printf("DBG in recv_tmsg(): Received %d bytes\n", res);
#endif
	if (!res) //FIN received
		return 0;

	msg->len = ntohl(*(int *)msg_hdr);
	msg->msgcode = msg_hdr[4];

	//let's allocate space and read the remaining bytes
	if (msg->len > 5) {
		tmp = (char *)malloc(msg->len - 5);
		res = recv(sockfd, tmp, msg->len - 5, MSG_WAITALL);
		if (res == -1) {
			perror("recv() while receiving the second half of the message: ");
			exit(1);
		}
#ifdef DEBUG
		printf("DBG: in recv_tmsg(): Received %d bytes\n", res);
#endif
	}

	switch (msg->msgcode) {
		case HELLO :
			((struct thelloreq *)msg)->udp = ntohs(*((short *)tmp));
			payload_len = msg->len - 7;
			((struct thelloreq *)msg)->nick = (char *)malloc(payload_len + 1); //reserving space for terminating char
			strncpy(((struct thelloreq *)msg)->nick,&tmp[2], payload_len);
			((struct thelloreq *)msg)->nick[payload_len] = '\0';
			break;

		case ERROR :
			payload_len = msg->len -5;
			((struct terror *)msg)->str = (char *)malloc(payload_len + 1);
			strncpy(((struct terror *)msg)->str, tmp, payload_len);
			((struct terror *)msg)->str[payload_len] = '\0';
			break;

		case WHOREP :
			((struct twhorep *)msg)->usercount = ntohs(*((short *)tmp));
			payload_len = msg->len - 7;
			((struct twhorep *)msg)->list = (char *)malloc(payload_len + 1);
			strncpy(((struct twhorep *)msg)->list, &tmp[2], payload_len);
			((struct twhorep *)msg)->list[payload_len] = '\0';
			break;

		case ADDRREQ :
			payload_len = msg->len - 5;
			((struct taddrreq *)msg)->uname = (char *)malloc(payload_len + 1);
			strncpy(((struct taddrreq *)msg)->uname, tmp, payload_len);
			((struct twhorep *)msg)->list[payload_len] = '\0';
			break;

		case ADDRREP :
			((struct taddrrep *)msg)->ip = *((int *)tmp);
			((struct taddrrep *)msg)->port = ntohs(*((short *)(tmp + 4)));
			break;

		case MATCHREQ :
			((struct tmatchreq *)msg)->ip = *((int *)(tmp));
			((struct tmatchreq *)msg)->port = ntohs(*((short *)(tmp + 4)));
			payload_len = msg->len - 11;
			((struct tmatchreq*)msg)->from = (char *)malloc(payload_len + 1);
			strncpy(((struct tmatchreq *)msg)->from, &tmp[6], payload_len);
			((struct tmatchreq *)msg)->from[payload_len] ='\0';
			break;

		case MATCHREP :
			((struct tmatchrep *)msg)->accepted = tmp[0];
			break;

		case HIT :
			((struct thit *)msg)->square = tmp[0];
			break;

		case OK :
		case WHOREQ :
		case DISCONNECT :
		case AVAILABLE :
			break;

		default:
			return -1;
	}
	return res;
}

# define _TRISMESSAGE_H
#endif
