#ifndef _STRING_H
# include <string.h>
#endif

#ifndef _STDLIB_H
# include <stdlib.h>
#endif

#ifndef _TCLIENT_H
# include "tclient.h"
#endif

#ifndef _LIST_H

/*
 * Creating the new type List
 */
typedef struct elem *List;

/*
 * Creating a list element
 */
struct elem {
	struct tclient *c;
	List next;
};

/**
 * @function lalloc Allocator for a list element
 */
List lalloc()
{
	return (List )malloc(sizeof(struct elem));
}

/**
 * @function lfree Deallocator for a list
 * @param head pointer to list head. It will be set to zero when lfree will complete.
 */
void lfree(List *head)
{
	List p;
	while (*head) {
		p = *head;
		*head = (*head)->next;
		tcfree(&(p->c));
		free(p);
	}
}

/**
 * @function linsert Insert an element behind the last element
 * @param lhead pointer to the list
 * @param src pointer to information
 * @return 0 if ok, -1 if an error occurs
 */
int linsert(List *head, const int ip, const int sockfd)
{
	List *p;
	List new_elem =  lalloc();

	if (!new_elem)
		return -1;

	new_elem->c = tcalloc();
	init_tclient(new_elem->c, ip, sockfd);
	new_elem->next = NULL;

	//Linus way...
	for (p = head; *p; p = &((*p)->next));
	*p = new_elem;

	return 0;
}

/**
 * @lremove Remove an element from list
 * @param lhead List head
 * @param str string to remove
 * @return 0 if ok, -1 if the element was not found
 */
int lremove(List *head, const char *str)
{
	List *pp;
	List q;
	int len;
	char *nick;

	len = strlen(str);

	for (pp = head; *pp; pp = &(*pp)->next) {
		nick = (*pp)->c->nick;
		if (!strncmp(nick, str, ((len < strlen(nick)) ? len : strlen(nick)) + 1)) {
			q = *pp;
			*pp = (*pp)->next;
			//tcfree(&q->c);
			q->next = NULL;
			lfree(&q);
			return 0;
		}
	}

	return -1; // string not found
}

/**
 * @function gettclient
 * @param str the nickname we're looking for.
 * @param head the list head.
 * @return a copy of the tclient from the list if it exist null otherwise.
 */
struct tclient *gettclient_bynick(const List head, const char *str)
{
	List p;
	int l1 = strlen(str), l2;

	for (p = head; p; p = p->next) {
		if (!p->c->nick)
			continue;

		l2 = strlen(p->c->nick);
		if (!strncmp(p->c->nick, str, ((l1 < l2) ? l1 : l2) + 1)) //client found
			return p->c;
	}
	return NULL;
}

/**
 * @function gettclient
 * @param head the list head.
 * @param str the nickname we're looking for.
 * @return a pointer to the tclient found. NULL if not found
 */
struct tclient *gettclient_bysockfd(const List head, const int sockfd)
{
	List p;
	for (p = head; p && p->c->sockfd != sockfd; p = p->next);
	return p->c;
}

/**
 * Convert the list into an array
 *
 * head is the head of the list, res is a pointer to pointer to the destinating
 * array. res is modified and its space must be freed.
 * Returns the element's number of the array
 */
int listtoarray(List head, struct tclient **res)
{
	int i, count = 0;
	List p;
	for (p = head; p; p = p->next)
		count++;
	*res = (struct tclient *)malloc(sizeof(struct tclient)*count);
	if (!(*res))
		return 0;

	p = head;
	for (i = 0; i < count; i++) {
		tccpy(&(*res)[i], *(p->c));
		p = p->next;
	}
	return count;
}

/*
 * getnicks
 * head is the list head
 * dest is a pointer to an array of char where the result is stored
 *
 * Returns the number of element in the array. It returns only the client who \
 * has succesfully logged in!
 */
int getnicks(List head, char ***dst)
{
	int i, count = 0;
	List p;
	for (p = head; p; p = p->next)
		if (p->c->nick)
			count++;

	p = head;
	*dst = malloc(sizeof(char *)*count);
	for (i = 0; i < count; i++) {
		if (p->c->nick)
			(*dst)[i] = p->c->nick; //No string copy
		p = p->next;
	}

	return count;
}

#define _LIST_H
#endif
