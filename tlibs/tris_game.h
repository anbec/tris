#ifndef _STDIO_H
# include <stdio.h>
#endif

#ifndef _TRIS_GAME_H

#define INITCHAR '-'

/*
 * This struct contains all the match'es informations
 */
struct tris_game {
	char board[3][3];
	char next_symbol;
	char hitcounter;
};

/*
 * Initialize a match
 */
void init_tgame(struct tris_game *tg)
{
	int i, j;
	if (!tg)
		return;

	for (i = 0; i < 3; i++)
		for (j = 0; j < 3; j++)
			tg->board[i][j] = INITCHAR;

	tg->next_symbol = 'X';
	tg->hitcounter = 0;
}

/*
 * Check column for a tris.
 * Returns -1 if col is out of bound (greater than 2 or less than 0), 1 if this
 * function find a tris, 0 otherwise.
 */
int check_column(const struct tris_game *tg, const int col)
{
	if (col < 0 || col > 2)
		return -1;

	if (tg->board[0][col] != INITCHAR && \
		tg->board[0][col] == tg->board[1][col] && \
		tg->board[0][col] == tg->board[2][col])
		return 1;
	return 0;
}

/*
 * Check row for a tris.
 * Returns -1 if row is out of bound (greater than 2 or less than 0), 1 if this
 * function find a tris, 0 otherwise.
 */
int check_row(const struct tris_game *tg, const int row)
{
	if (row < 0 || row > 2)
		return -1;

	if (tg->board[row][0] != INITCHAR && \
		tg->board[row][0] == tg->board[row][1] && \
		tg->board[row][0] == tg->board[row][2])
		return 1;
	return 0;
}

/*
 * Check diagonal for a tris.
 * Returns -1 if the indexes are out of bound (greater than 2 or less than 0), \
 * 1 if this function find a tris, 0 otherwise.
 */
int check_diagonal(const struct tris_game *tg, const int row, const int col)
{
	if (row < 0 || col < 0 || row > 2 || col > 2)
		return -1;

	if (tg->board[0][0] != INITCHAR && tg->board[0][0] == tg->board[1][1] && tg->board[1][1] == tg->board[2][2])
		return 1;

	if (tg->board[0][2] != INITCHAR && tg->board[0][2] == tg->board[1][1] && tg->board[1][1] == tg->board[2][0])
		return 1;

	return 0;
}

/*
 * Put a mark on the square
 * Returns 1 whenever the player wins, 0 otherwise or 2 if there's no winner and
 * no possible moves are left.
 * It returns -1 if you try to mark an already marked square or -2 if square is
 * not valid.
 */
int hit(struct tris_game *tg, const int square)
{
	if (square < 1 || square > 9)
		return -2;

	int row, col;
	switch (square) {
		case 1:
		case 2:
		case 3:
			row = 2;
			break;
		case 4:
		case 5:
		case 6:
			row = 1;
			break;
		case 7:
		case 8:
		case 9:
			row = 0;
			break;
	}

	col = (square - 1)%3;

	if (tg->board[row][col] != INITCHAR) //square already marked
		return -1;

	tg->hitcounter++;
	tg->board[row][col] = tg->next_symbol;
	tg->next_symbol = (tg->next_symbol == 'X') ? 'O' : 'X';
	if (check_column(tg, col) == 1 || check_row(tg, row) == 1)
		return 1;
	if ((row + col)%2 == 0)
		if (check_diagonal(tg, row, col) == 1)
			return 1;
	if (tg->hitcounter == 9)
		return 2;
	return 0;
}

/*
 * Prints out the tris board
 */
void print_board(const struct tris_game *tg)
{
	int i, j;
	for (i = 0; i<3; i++) {
		for (j = 0; j < 3; j++) {
			printf(" %c %c", tg->board[i][j], ( j == 2) ? '\n' : '|');
		}
		printf( "%s", (i != 2) ? "___|___|___\n" :"   |   |\n");
	}
}

#define TRIS_GAME_H
#endif
