#ifndef _STRING_H
# include <string.h>
#endif

#ifndef _STDLIB_H
# include <stdlib.h>
#endif

#ifndef _TCLIENT_H

enum tcstatus { FREE, BUSY };

/*
 * Struct containing client information
 */
struct tclient {
	char *nick;
	int ip_addr, sockfd;
	short udpport;
	enum tcstatus status;
	struct tclient *waiting;
};

/**
 * Initialize a tclient
 */
struct tclient *init_tclient(struct tclient *tc, const int ip, const int sockfd)
{
	tc->nick = NULL;
	tc->ip_addr = ip;
	tc->status = BUSY;
	tc->sockfd = sockfd;
	tc->waiting = NULL;
	tc->udpport = 0;
	return tc;
}

/**
 * client allocator
 */
struct tclient *tcalloc()
{
	return (struct tclient *)malloc(sizeof(struct tclient));
}

/**
 * client deallocator
 */
void tcfree(struct tclient **tc)
{
	free((*tc)->nick);
	free(*tc);
	*tc = NULL;
}

/*
 * Create a copy for tclient structure into an empty one.
 */
void tccpy(struct tclient *dest, const struct tclient src)
{
	dest->nick = (char *)malloc(strlen(src.nick) + 1);
	strncpy(dest->nick, src.nick, strlen(src.nick));
	dest->nick[strlen(src.nick)] = '\0';

	dest->ip_addr = src.ip_addr;

	dest->udpport = src.udpport;

	dest->status =src.status;

	dest->waiting = src.waiting;
}


#define _TCLIENT_H
#endif
