#ifndef _STRING_H
# include <string.h>
#endif

#ifndef _STDLIB_H
# include <stdlib.h>
#endif

#ifndef _TCLIENT_H
# include "tclient.h"
#endif

#ifndef _LIST_H
# include "list.h"
#endif

#ifndef _BENCODE_H
# include "../tlibs/bencode.h"
#endif

#ifndef _TCOMMON_H

/*
 * Define connections between msg types and their representation
 */
#define HELLO 1
#define WHOREQ 2
#define WHOREP 3
#define ADDRREQ 4
#define ADDRREP 5
#define MATCHREQ 6
#define MATCHREP 7
#define HIT 8
#define DISCONNECT 9
#define AVAILABLE 10
#define OK 11
#define ERROR 12

/*
 * The next structure is used to store a tris message in an easy to access form.
 * It's just a wrapper for msgcode's specific message
 */
struct trismsg {
	int len; //the entire message length (msg code + payload).
	char msgcode;
	char arguments[10]; //arguments space
};

/*
 * List of possible messages
 */
struct thelloreq {
	int len;
	char msgcode;
	short udp;
	char *nick;
};

struct terror {
	int len;
	char msgcode;
	char *str;
};

struct tok {
	int len;
	char msgcode;
};

struct twhoreq {
	int len;
	char msgcode;
};

struct twhorep {
	int len;
	char msgcode;
	short usercount;
	char *list;
};

struct taddrreq {
	int len;
	char msgcode;
	char *uname;
};

struct taddrrep {
	int len;
	char msgcode;
	int ip;
	short port;
};

struct tmatchreq {
	int len;
	char msgcode;
	int ip;
	short port;
	char *from;
};

struct tmatchrep {
	int len;
	char msgcode;
	char accepted;
};

struct thit {
	int len;
	char msgcode;
	char square;
};

struct tdisconnect {
	int len;
	char msgcode;
};

struct tavailable {
	int len;
	char msgcode;
};

/**
 * @function create_tmsg
 * The destinaton memory space must be free after usage
 *
 * @param dst pointer to pointer to destination message
 * @param msg a pointer to a struct trismsg
 *
 * @return the dst's size in bytes
 */
int create_tmsg(char **dst, const struct trismsg *msg)
{
	*dst = (char *)malloc(msg->len);
	*(int *)(*dst) = htonl(msg->len);
	(*dst)[4] = msg->msgcode;

	switch (msg->msgcode) {
		case HELLO :
			*((short *)(*dst + 5)) = htons(((struct thelloreq *)msg)->udp);
			strncpy((*dst + 7), ((struct thelloreq *)msg)->nick, strlen(((struct thelloreq *)msg)->nick));
			break;

		case ERROR :
			strncpy(*dst + 5, ((struct terror *)msg)->str, strlen(((struct terror *)msg)->str));
			break;

		case WHOREP :
			*((short *)(*dst + 5)) = htons(((struct twhorep *)msg)->usercount);
			strncpy(*dst + 7, ((struct twhorep *)msg)->list, strlen(((struct twhorep *)msg)->list));
			break;

		case ADDRREQ :
			strncpy(*dst + 5, ((struct taddrreq *)msg)->uname, strlen(((struct taddrreq *)msg)->uname));
			break;

		case ADDRREP :
			*((int *)(*dst + 5)) = ((struct taddrrep *)msg)->ip;
			*((short *)(*dst + 9)) = htons(((struct taddrrep *)msg)->port);
			break;

		case MATCHREQ :
			strncpy(*dst + 11, ((struct tmatchreq *)msg)->from, strlen(((struct tmatchreq *)msg)->from));
			*((int *)(*dst + 5)) = ((struct tmatchreq *)msg)->ip;
			*((short *)(*dst + 9)) = htons(((struct tmatchreq *)msg)->port);
			break;

		case MATCHREP :
			(*dst)[5] = ((struct tmatchrep *)msg)->accepted;
			break;

		case HIT :
			*((char *)(*dst + 5)) = ((struct thit *)msg)->square;
			break;

		case OK :
		case WHOREQ :
		case DISCONNECT :
		case AVAILABLE :
			break;

		default:
			return -1;
	}

	return msg->len;
}

#define _TCOMMON_H
#endif	//#ifndef _TCOMMON_H
