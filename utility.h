#ifndef _MATH_H
# include <math.h>
#endif

#define _UTILITY_H

/**
 * @function ddigit Calculates the number of decimal digit needed to represente\
 *  the given number n
 * @param n the number to compute
 * @return number of digit
 */
int ddigit(const int n)
{
	return (int)ceil(log10(n + 1));
}
