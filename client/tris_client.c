#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "../tlibs/trismessage.h"
#include "../tlibs/common.h"
#include "../tlibs/tris_game.h"

#define SA struct sockaddr
#define STM struct trismsg

#define NORMALPC '>'
#define MATCHPC '#'

#define handle_err(msg) \
	do { perror(msg); exit(0); } while(0)

#define MIN(a, b) \
	(a < b) ? a : b

#define INIT_TIMEVAL(t, s) \
	do { t.tv_sec = s; t.tv_usec = 0; } while(0)

#define MAX_PEER_WAIT 60

void print_help() {
	int fd, res;
	char buf[256];
	fd = open("cmd_list.txt", O_RDONLY);
	if (fd == -1)
		handle_err("open(): ");

	printf("Sono disponibili i seguenti comandi:\n");
	while ((res = read(fd, buf, sizeof(buf))) > 0) {
		write(0, buf, res);
	}
	if (res == -1)
		handle_err("read(): ");

	printf("\n");
	fflush(stdout);

	res = close(fd);
	if (res == -1)
		handle_err("close(): ");
}

/*
 * Creates a udp connected socket with given values
 */
void init_udp_socket(int *sockfd, struct sockaddr_in * addr, short port, int ip)
{
	int res;
	/*
	* Creare il socket e prepararlo a ricevere i dati dal peer
	*/
	*sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	memset(addr, 0, sizeof(*addr));
	addr->sin_family = AF_INET;
	addr->sin_port = htons(port);
	//peer_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	addr->sin_addr.s_addr = ip;

	/*
	* Creating a connected udp socket
	*/
	res = connect(*sockfd, (SA *)addr, sizeof(*addr));
	if (res == -1)
		handle_err("connect() ");
}

/*
 * Check if the command can be executed
 */

/*
 * Creates and send an AVAILABLE message to the server
 */
int send_available(int sockfd)
{
	struct trismsg msg;
	char *tmp;
	int len, res;

	msg.len = 5;
	msg.msgcode = AVAILABLE;

	len = create_tmsg(&tmp, &msg);
	res = send(sockfd, tmp, len, 0);
	if (res == -1)
		handle_err("send(): ");

	free(tmp);
	return res;
}

/*
 * Controls extended functionality
 * Returns 0 if ok, -1 if cmd is not allowed
 */
int check_cmd(const char *cmd, enum tcstatus s)
{
	int len;
	if (s == BUSY)
		return 0;

	len = strlen(cmd);

	if (!strncmp(cmd, "!show_map", (MIN(strlen("!show_map"), len)) + 1))
		return -1;
	if (!strncmp(cmd, "!hit", (MIN(strlen("!hit"), len)) + 1))
		return -1;
	return 0;
}

int main(int argc, char **argv)
{
	int res, srv_sd, udp_sender_sd, udp_receiver_sd, i, maxfd, peer_ip, square;
	unsigned int len;
	short srv_port, my_udp_port, peer_port;
	char nick[20], *tmp, cmd[32], line[64], promptc = NORMALPC, *out, ans = 0, peer_msg[6], client_name[20];
	char **peers;
	struct sockaddr_in srv_addr, my_addr, peer_addr;
	struct thelloreq hello;
	struct trismsg msg;
	struct tris_game tg;
	enum tcstatus my_status = FREE;
	fd_set masterset, readset;
	struct timeval *pt, t;
	pt = NULL;
	FD_ZERO(&masterset);
	FD_ZERO(&readset);

	if (argc < 3) {
		printf("Utilizzo: %s <host remoto> <porta>\n", argv[0]);
		exit(1);
	}

	sscanf(argv[2], "%hd", &srv_port);
	printf("Indirizzo: %s (Porta: %d)\n", argv[1], srv_port);

	/*
	 * First establish a tcp connection with the server...
	 */
	memset(&my_addr, 0, sizeof(my_addr));
	srv_addr.sin_family = AF_INET;
	srv_addr.sin_port = htons(srv_port);
	res = inet_pton(AF_INET, argv[1], &srv_addr.sin_addr.s_addr);
	if (res == -1)
		handle_err("inet_pton(): ");

	srv_sd = socket(AF_INET, SOCK_STREAM, 0);
	if (srv_sd == -1)
		handle_err("socket(): ");

	res = connect(srv_sd, (SA *)&srv_addr, sizeof(srv_addr));
	if (res == -1)
		handle_err("connect(): ");

	printf("\nConnessione al server %s (porta %d) effettuata con successo\n\n", argv[1], srv_port);

	print_help();

	do {
		/*
		* Asking for nickname and UDP port number
		*/
		printf("Inserisci il tuo nome: ");
		fgets(line, sizeof(line), stdin);
		sscanf(line, "%s", nick);
#ifdef DEBUG
		printf("string read = %s\n", nick);
#endif
		printf("Inserisci la porta UDP di ascolto: ");
		fgets(line, sizeof(line), stdin);
		sscanf(line, "%hd", &my_udp_port);
#ifdef DEBUG
		printf("string read = %hd\n", my_udp_port);
#endif

		/*
		* Preparing HELLO
		*/
		memset(&hello, 0, sizeof(hello));
		hello.len = bencstr(&(hello.nick), nick) + sizeof(hello.len) + sizeof(hello.udp) + sizeof(hello.msgcode);
		hello.msgcode = HELLO;
		hello.udp = my_udp_port;

		len = create_tmsg(&tmp, (STM *)&hello);

		/*
		* Sending HELLO
		*/
		res = send(srv_sd, tmp, len, 0);
		if (res == -1)
			handle_err("send(): ");
		/*
		* Checking for OK
		*/
		res = recv_tmsg(srv_sd, &msg);
		if (msg.msgcode == ERROR) {
			bdecstr(&tmp, ((struct terror* )&msg)->str);
			printf("Errore durante la registrazionen presso il server: %s", tmp);
			bstring_free(&tmp);
		}
	} while (msg.msgcode != OK);

	/*
	 * Creating the udp receiver socket
	 */
	memset(&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	my_addr.sin_port = htons(my_udp_port);
	my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	/*res = inet_pton(AF_INET, argv[1], &my_addr.sin_addr.s_addr);
	if (res == -1)
		handle_err("inet_pton() preparing udp socket: ");
	*/
	udp_receiver_sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (udp_receiver_sd == -1)
		handle_err("socket() while preparing udp receiver socket: ");

	res = bind(udp_receiver_sd, (SA *)&my_addr, sizeof(my_addr));
	if (res == -1)
		handle_err("bind() on udp receiver socket: ");

	FD_SET(0, &masterset); //stdin
	FD_SET(srv_sd, &masterset);
	FD_SET(udp_receiver_sd, &masterset);
	maxfd = (srv_sd > udp_receiver_sd) ? srv_sd : udp_receiver_sd;

	/*
	 * and the sender socket
	 */
	udp_sender_sd = socket(AF_INET, SOCK_DGRAM, 0);
	if (udp_sender_sd == -1)
		handle_err("socket(): while creating sender socket: ");

	while (1) {
		/*
		 * Prints command prompt
		 */
		readset = masterset;
		if (FD_ISSET(0, &masterset)) //prints '>' only if the client is accepting user's command
			write(1, &promptc, 1);
		res = select(maxfd + 1, &readset, NULL, NULL, pt);
		if (res == -1)
			handle_err("select(): ");

		if (res == 0) { //timeout
			INIT_TIMEVAL(t, MAX_PEER_WAIT);

			promptc = NORMALPC;
			my_status = FREE;
			send_available(srv_sd);
			if (FD_ISSET(0, &masterset)) {
				printf("\nTempo scaduto: HAI PERSO!\n");
			} else {
				printf("Tempo scaduto: HAI VINTO!\n");

				FD_SET(0, &masterset);
			}
			fflush(stdout);
			pt = NULL;
			continue;
		}


		for (i = 0; i <= maxfd; i++) {
			if (FD_ISSET(i, &readset)) {
				if (!i) { //stdin

					/*
					 * Read and execute a command from stdin
					 */
					fgets(line, sizeof(line), stdin);
					sscanf(line, "%s", cmd);

					if (!strncmp(line, "\n", MIN(strlen(line), 1))) { //empty line
#ifdef DEBUG
						printf("DBG: NEW LINE READ!\n");
						fflush(stdout);
#endif
						continue;
					} else {
#ifdef DEBUG
						printf("DBG: read line = %sDBG: command = %s\n", line, cmd);
						fflush(stdout);
#endif
					}

					len = strlen(cmd);

					if (!strncmp(cmd, "!help", MIN(len, strlen("!help")))) {	//!help
						print_help();

					} else if (!strncmp(cmd, "!who", MIN(len, strlen("!who")))) {	//!who
						msg.len = sizeof(msg.len) + sizeof(msg.msgcode);
						msg.msgcode = WHOREQ;

						res = create_tmsg(&tmp, &msg);

						res = send(srv_sd, tmp, res, 0);
						if (res == -1)
							handle_err("send(): ");
						free(tmp);

						FD_CLR(0, &masterset);

					} else if (!strncmp(cmd, "!connect", MIN(len, strlen("!connect")))) {	//!connect

						//extract nickname from line
						res = sscanf(line, "%*s%s", client_name);
#ifdef DEBUG
						printf("DBG: connect's argument = %s\n", client_name);
						fflush(stdout);
#endif
						if (res != 1) { //no argument found!
							printf("Utilizzo: !connect <nome_client>\n");
							fflush(stdin);
							break;
						}

						if (!strncmp(client_name, nick, MIN(strlen(client_name), strlen(nick)))) {
							printf("Non puoi giocare contro te stesso.\n");
							fflush(stdin);
							break;
						}

						msg.len = bencstr(&(((struct taddrreq *)&msg)->uname), client_name) + \
							sizeof(msg.len) + sizeof(msg.msgcode);
						msg.msgcode = ADDRREQ;

						res = create_tmsg(&tmp, &msg);
						bstring_free(&(((struct taddrreq *)&msg)->uname));
						//sending request
						res = send(srv_sd, tmp, res, 0);
						free(tmp);

						FD_CLR(0, &masterset);
					} else if (!strncmp(cmd, "!hit", MIN(len, strlen("!hit")))) { //hit
						if (check_cmd(cmd, my_status) == -1) {
							fprintf(stderr, "Comando valido solo in partita\n");
							fflush(stderr);
							break;
						}

						//extract square number
						sscanf(line, "%*s%d", &square);
#ifdef DEBUG
						printf("DBG: Command: hit, square = %d\n", square);
						fflush(stdout);
#endif
						res = hit(&tg, square);
						print_board(&tg);
						if (res == -2) {
							fprintf(stderr, "Sono ammessi numeri di casella da 1 a 9\n");
							fflush(stderr);
							break;
						}

						if (res == -1) {
							fprintf(stderr, "La casella %d è già stata marcata\n", square);
							fflush(stderr);
							break;
						}

						if (res > 0) {
							if (res == 1)
								printf("Hai vinto!\n");
							if (res == 2)
								printf("Pareggio, nessun vincitore\n");

							fflush(stdout);

							/*
							 * Sending information to the server
							 */
							my_status = FREE;
							promptc = NORMALPC;
							send_available(srv_sd);
							pt = NULL;
						} else {
							printf("E' il turno di %s\n", client_name);
							FD_CLR(0, &masterset);
						}

						msg.len = sizeof(msg.len) + sizeof(msg.msgcode) + sizeof(((struct thit *)&msg)->square);
						msg.msgcode = HIT;
						((struct thit *)&msg)->square = (char)square;

						len = create_tmsg(&tmp, &msg);

						res = send(udp_sender_sd, tmp, len, 0);
						if (res == -1)
							handle_err("send(): ");

						free(tmp);

						//reset timer
						INIT_TIMEVAL(t, MAX_PEER_WAIT);

					} else if (!strncmp("!show_map", cmd, MIN(strlen("!show_map"), len) + 1)) { //show_map
						if (check_cmd(cmd, my_status) == -1) {
							fprintf(stderr, "Comando valido solo in partita\n");
							fflush(stderr);
							break;
						}

						print_board(&tg);
						fflush(stdout);
					} else if (!strncmp("!disconnect", cmd, MIN(strlen("!disconnect"), len) + 1)) { //disconnect
						printf("Ti sei arreso!\n");
						fflush(stdout);

						msg.len = 5;
						msg.msgcode = DISCONNECT;
						len = create_tmsg(&tmp, &msg);

						res = send(udp_sender_sd, tmp, len, 0);
						if (res == 1)
							handle_err("send(): ");

						free(tmp);

						FD_SET(0, &masterset);
						promptc = NORMALPC;
						my_status = FREE;

						send_available(srv_sd);
						pt = NULL;

					} else if (!strncmp("!quit", cmd, MIN(strlen("!quit"), len))) { //!quit
						if (my_status == BUSY) {
							msg.len = 5;
							msg.msgcode = DISCONNECT;
							len = create_tmsg(&tmp, &msg);


							res = send(udp_sender_sd, tmp, len, 0);
							if (res == 1)
								handle_err("send(): ");

							free(tmp);
							res = close(udp_sender_sd);
							if (res == -1)
								handle_err("close(): ");

							res = close(udp_receiver_sd);
							if (res == -1)
								handle_err("close(): ");
						}
						res = close(srv_sd);
						if (res == -1)
							handle_err("close(): ");
						exit(0);
					} else {
						out = "Comando sconosciuto\n";
						res = write(1, out, strlen(out));
						if (res == -1)
							handle_err("write(): ");
					}

				} else if (i == srv_sd) {

					res = recv_tmsg(srv_sd, &msg);

					if (!res) { //FIN
						fprintf(stderr, "Server non raggiungibile\n");
						exit(1);
					}

					switch(msg.msgcode) {
						case WHOREP :
							len = (int)((struct twhorep *)&msg)->usercount;
							peers = malloc(sizeof(char *)*len);
							extract_benclist(peers, ((struct twhorep *)&msg)->list);

							out = "Client connessi al server: ";
							res = write(1, out, strlen(out));
							if (res == -1)
								handle_err("write(): ");

							for (i = 0; i < len; i++)
								printf("%s\t", peers[i]);
							printf("\n");

							FD_SET(0, &masterset);
							break;

						case ADDRREP:
							peer_port = ((struct taddrrep *)&msg)->port;
							peer_ip = ((struct taddrrep *)&msg)->ip;

							init_udp_socket(&udp_sender_sd, &peer_addr, peer_port, peer_ip);
#ifdef DEBUG
							printf("DBG: Match accepted. Received udp port: %hd\n", peer_port);
							fflush(stdout);
#endif

							/*
							* Adding the udp socket to masterset for select syscall,
							* updating maxfd and re-enabling stdin.
							*/
							FD_SET(0, &masterset);

							printf("%s ha accettato la partita\n", client_name);


							init_tgame(&tg);
							my_status = BUSY;
							promptc = MATCHPC;
							INIT_TIMEVAL(t, MAX_PEER_WAIT);
							pt = &t;

							printf("Partita avviata con %s\n", client_name);
							printf("Il tuo simbolo è: X\nE' il tuo turno:\n");
							fflush(stdout);

							break;

						case ERROR :
							bdecstr(&tmp, ((struct terror *)&msg)->str);
							printf("Errore: %s\n", tmp);
							fflush(stdin);
							free(tmp);
							FD_SET(0, &masterset);
							break;

						case MATCHREQ:
							bdecstr(&tmp, ((struct tmatchreq *)&msg)->from);
							printf("\n%s vuole giocare con te.\nVuoi accettare la partita? [S/n] ", tmp);
							fflush(stdout);
							strncpy(client_name, tmp, 20);
							client_name[19] = '\0';
							free(tmp);

							do {
								fgets(line, sizeof(line), stdin);
								sscanf(line, "%c", &ans);
								if (ans == 's' || ans == '\n') {

									peer_port = ((struct tmatchreq *)&msg)->port;
									peer_ip = ((struct tmatchreq *)&msg)->ip;

									((struct tmatchrep *)&msg)->accepted = 'T';
#ifdef DEBUG
									printf("Connected to peer port = %hd\n", peer_port);
									fflush(stdout);
#endif

									init_udp_socket(&udp_sender_sd, &peer_addr, peer_port, peer_ip);

									/*
									 * Initializing playing
									 */
									init_tgame(&tg);

									promptc = MATCHPC;
									FD_CLR(0, &masterset);
									my_status = BUSY;
									INIT_TIMEVAL(t, MAX_PEER_WAIT);
									pt = &t;

									printf("Hai accettato la partita.\nE' il turno di %s\n", client_name);
									fflush(stdout);

								} else if (ans == 'n') {
									((struct tmatchrep *)&msg)->accepted = 'F';
									printf("Hai rifiutato la partita.\n");
									fflush(stdout);
								} else {
									printf("Digita 's' per accettare o 'n' per rifiutare ");
									fflush(stdout);
								}
							} while (ans != '\n' && ans != 's' && ans != 'n');
							msg.len = sizeof(msg.len) + sizeof(msg.msgcode) + sizeof(((struct tmatchrep *)&msg)->accepted);
							msg.msgcode = MATCHREP;

							len = create_tmsg(&tmp, &msg);

							res = send(srv_sd, tmp, len, 0);
							if (res == -1)
								handle_err("send(): ");
							free(tmp);

							break;
					}
				} else { //message available on udp receiver socket
					len = sizeof(peer_addr);
					res = recvfrom(udp_receiver_sd, (void *)peer_msg, sizeof(peer_msg), MSG_WAITALL, (SA *)&peer_addr, &len);
					if (res == -1)
						handle_err("recvfrom(): ");

					if (len > sizeof(peer_addr)) {
						fprintf(stderr, "recvfrom(): the src addr have been truncated (not enough space in buffer)\n");
						exit(1);
					}
#ifdef DEBUG
					printf("DBG: Received message from peer\n");
					fflush(stdout);
#endif
					switch (peer_msg[4]) {
						case HIT:
							res = hit(&tg, (int)peer_msg[5]);
							print_board(&tg);

							if (res > 0) {
								if (res == 1)
									printf("%s ha vinto la partita\n", client_name);
								if (res == 2)
									printf("Pareggio, nessun vincitore\n");
								fflush(stdout);
								promptc = NORMALPC;
								my_status = FREE;
								pt = NULL;

								/*
								 * Send information to the server
								 */
								send_available(srv_sd);
							} else {
								printf("E' il tuo turno:\n");
								INIT_TIMEVAL(t, MAX_PEER_WAIT);
							}

							fflush(stdout);
							FD_SET(0, &masterset);
							break;
						case DISCONNECT :
							printf("%s si è disconnesso. Hai vinto la partita!\n", client_name);
							fflush(stdout);

							my_status = FREE;
							promptc = NORMALPC;
							FD_SET(0, &masterset);

							send_available(srv_sd);
							pt = NULL;
					}//switch (peer_msg[4])

				}
				break;
			} //if (FD_ISSET(i, &readset)
		} //for (i = 0; i < maxfd; i++)

		FD_ZERO(&readset);

	} //while (1)
	return 0;
}
