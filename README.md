This is the README file for the tris project

Tris is an hybrid client-server/peer-to-peer network application.
It lets you play the game of Tris.

How to build
------------
	cd client
	make

	cd ../server
	make

How to play
-----------

1. Start server with

		./tris_server <server's ip address> <server's listening port>

2. Start client with

		./tris_client <server's ip address> <server's listening port>

	and provide the port where your client will wait for connections
