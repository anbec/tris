#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <sys/select.h>
#include <sys/time.h>

#include "../tlibs/list.h"
#include "../tlibs/common.h"
#include "../tlibs/trismessage.h"

#define SA struct sockaddr
#define STM struct trismsg
#define CONNQUEUE 100

#define handle_err(msg) \
	do { perror(msg); exit(1); } while(0)

int main(int argc, char **argv)
{
	char *my_ip, *tmp, **nick_array;
	short port;
	int listener, res, val, new_sd, maxfd, i;
#ifdef DEBUG
	const char *tmp2;
	int j;
#endif
	unsigned int len;
	struct sockaddr_in my_addr, client_addr;
	fd_set masterset, readset;
	struct trismsg msg;
	struct tclient *tc, *tc_tmp;

	/*
	 * list of all the connected client
	 */
	List clientl = NULL;

	/*
	 * Clear the sets
	 */
	FD_ZERO(&masterset);
	FD_ZERO(&readset);

	if (argc < 3) {
		fprintf(stderr, "Utilizzo: %s <host> <porta>\n", argv[0]);
		exit(1);
	}

	/*
	 * Retriving srv ip address and udp port
	 */
	res = sscanf(argv[2], "%hd", &port);
	if (res < 1)
		handle_err("sscanf(): ");

	my_ip = malloc(strlen(argv[1]) + 1);
	strncpy(my_ip, argv[1], strlen(argv[1]));
        my_ip[strlen(argv[1])] = '\0';

        printf("Indirizzo: %s (Porta: %hd)\n", my_ip, port);

	/*
	 * preparing my_addr
	 */
	memset(&my_addr, 0, sizeof(my_addr));
	my_addr.sin_family = AF_INET;
	//my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	my_addr.sin_port = htons(port);

	res = inet_pton(AF_INET, my_ip, &my_addr.sin_addr.s_addr);
	if (res == -1)
		handle_err("inet_pton():");

	/*
	 * Creating listening socket
	 */
	listener = socket(AF_INET, SOCK_STREAM, 0);
	if (listener == -1)
		handle_err("main(): Error creating listening socket");

	maxfd = listener;
	FD_SET(listener, &masterset);

	/*
	 * Setting SO_REUSEADDR socket's option
	 */
	val = 1;	//a non zero value to set a boolean socket's option
	res = setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val));
	if (res == -1)
		handle_err("setsockopt(): Error setting socket option");

	/*
	 * Binding listening socket
	 */
	bind(listener, (SA *)&my_addr, sizeof(my_addr));
	if (res == -1)
		handle_err("inet_pton(): ");

	/*
	 * mark listener as a passive socket (listening socket)
	 */
	res = listen(listener, CONNQUEUE);
	if (res == -1)
		handle_err("listen(): ");

	while(1) {
		readset = masterset;
		res = select(maxfd + 1, &readset, NULL, NULL, NULL);
		if (res == -1)
			handle_err("select(): ");

		/*
		 * Check which descriptor is ready
		 */
		for (i = 0; i <= maxfd; i++) {
			if (FD_ISSET(i, &readset)) { //found fd i ready

				if (i == listener) { //new connection

					len = sizeof(client_addr);
					new_sd = accept(listener, (SA *)&client_addr, &len);
					if (new_sd == -1)
						handle_err("accept(): ");

					/*
					 * saving client ip and socket
					 */
					linsert(&clientl, client_addr.sin_addr.s_addr, new_sd);

					printf("Connessione stabilita con il client\n");
					FD_SET(new_sd, &masterset);

					/*
					 * update maxfd
					 */
					if (maxfd < new_sd)
						maxfd = new_sd;

					FD_ZERO(&readset);
					break;
				} else {
					res = recv_tmsg(i,&msg);
					/*
					 * Manage closed connection
					 */
					if (!res) { //FIN
						res = close(i);
						if (res == -1)
							handle_err("close(): ");

						/*
						 * remove this client from the list
						 */
						tc = gettclient_bysockfd(clientl, i);
						printf("%s si è disconnesso dal server\n", tc->nick);
						lremove(&clientl, tc->nick);

						FD_CLR(i, &masterset);
						FD_ZERO(&readset);
						break;
					}

					switch (msg.msgcode) {
						case HELLO :
							bdecstr(&tmp, ((struct thelloreq *)&msg)->nick);
							tc_tmp = gettclient_bynick(clientl, tmp);
							tc = gettclient_bysockfd(clientl, i);

							if (!tc_tmp) { //it's a valid nick
								tc->nick = tmp;
								tc->status = FREE;
								tc->udpport = ((struct thelloreq *)&msg)->udp;
								printf("%s si è connesso\n", tc->nick);

								/*
								* Preparing response
								*/
								msg.len = 5;
								msg.msgcode = OK;
								len = create_tmsg(&tmp, &msg);

								/*
								* sending OK
								*/
								res = send(i, tmp, len, 0);
								if (res == -1)
									handle_err("send() :");

								break;
							}

							msg.len = bencstr(&(((struct terror *)&msg)->str), "Nome utente già utilizzato\n") + sizeof(msg.len) + sizeof(msg.msgcode);
							msg.msgcode = ERROR;
							len = create_tmsg(&tmp, &msg);
							res = send(i, tmp, len, 0);
							if (res == -1)
								handle_err("send(): ");
							break;

						case WHOREQ :
							/*
							 * preparing response
							 */
							res = getnicks(clientl, &nick_array);
#ifdef DEBUG
							for (j = 0; j < res; j++)
								printf("%d - %s\n", j, nick_array[j]);
#endif
							msg.len = create_benclist(&(((struct twhorep *)&msg)->list), (const char **)nick_array, res) + \
								sizeof(((struct twhorep *)&msg)->len) + sizeof(msg.msgcode) + \
								sizeof(((struct twhorep *)&msg)->usercount);

							((struct twhorep *)&msg)->usercount = (short)res;
							msg.msgcode = WHOREP;

							len = create_tmsg(&tmp, (STM *)&msg);

							/*
							 * send response
							 */
							res = send(i, tmp, len, 0);
							if (res == -1)
								handle_err("send(): ");
							break;

						case ADDRREQ :
							bdecstr(&tmp, ((struct taddrreq *)&msg)->uname);

							tc = gettclient_bynick(clientl, tmp);
							free(tmp);

							if (!tc) { //No client with requested nickname
								msg.len = bencstr(&(((struct terror *)&msg)->str), "Nome client inesistente") + sizeof(msg.len) + sizeof(msg.msgcode);
								msg.msgcode = ERROR;
								len = create_tmsg(&tmp, &msg);

								bstring_free(&(((struct terror *)&msg)->str));
								res = send(i, tmp, len, 0);
								if (res == -1)
									handle_err("send(): ");
								free(tmp);
								break;

							}

							if (tc->status == BUSY) { //requested client already busy
								msg.len = bencstr(&(((struct terror *)&msg)->str), "Client occupato in una partita") + sizeof(msg.len) + sizeof(msg.msgcode);
								msg.msgcode = ERROR;
								len = create_tmsg(&tmp, &msg);

								bstring_free(&(((struct terror *)&msg)->str));
								res = send(i, tmp, len, 0);
								if (res == -1)
									handle_err("send(): ");
								free(tmp);
								break;
							}

#ifdef DEBUG
							tmp = malloc(sizeof(char)*16);
							tmp2 = inet_ntop(AF_INET, (void *)&(tc->ip_addr), tmp, 16);
							//tmp2[15] = '\0';
							if (!tmp2)
								handle_err("inet_ntop(): ");

							printf("DBG: Connection request to %s received\n", tmp2);
							free(tmp);
							fflush(stdout);
#endif

							tc_tmp = gettclient_bysockfd(clientl, i);
							tc->waiting = tc_tmp;
							tc_tmp->status = BUSY;
							tc->status = BUSY;


							msg.len = bencstr(&(((struct tmatchreq *)&msg)->from), tc_tmp->nick) \
								+ sizeof(msg.len) + sizeof(msg.msgcode) +sizeof(((struct tmatchreq *)&msg)->ip) + \
								sizeof(((struct tmatchreq *)&msg)->port);
							msg.msgcode = MATCHREQ;
							((struct tmatchreq *)&msg)->ip = tc_tmp->ip_addr;
							((struct tmatchreq *)&msg)->port = tc_tmp->udpport;
#ifdef DEBUG
							tmp = malloc(sizeof(char)*16);
							if (!inet_ntop(AF_INET, (void *)&tc_tmp->ip_addr, tmp, 16))
								handle_err("inet_ntop(): ");

							printf("DBG: sent ip %s and port %d\n", tmp, tc_tmp->udpport);
							free(tmp);
							fflush(stdout);
#endif

							len = create_tmsg(&tmp, &msg);
							bstring_free(&((struct tmatchreq *)&msg)->from);

							res = send(tc->sockfd, tmp, len, 0);
							if (res == -1)
								handle_err("send() :");
							free(tmp);

#ifdef DEBUG
							printf("DBG: tmatchreq sent\n");
							fflush(stdout);
#endif
							break;

						case MATCHREP :
							tc = gettclient_bysockfd(clientl, i);
							if (((struct tmatchrep *)&msg)->accepted == 'F') {
								tc->status = FREE;
								tc_tmp = tc->waiting;
								tc_tmp->status = FREE;
								tc->waiting = NULL;

								/*
								 * Send an error to the requester
								 */
								msg.len = bencstr(&(((struct terror *)&msg)->str), "Il client non ha accettato la partita.") + \
									sizeof(msg.len) + sizeof(msg.msgcode);
								msg.msgcode = ERROR;
								len = create_tmsg(&tmp, &msg);
								bstring_free(&(((struct terror *)&msg)->str));
								res = send(tc_tmp->sockfd, tmp, len, 0);
								if (res == -1)
									handle_err("send() :");
								free(tmp);
#ifdef DEBUG
								printf("DBG: Error message sent\n");
								fflush(stdout);
#endif

								break;
							}

							/*
							 * Filling message's data
							 */
							msg.msgcode = ADDRREP;
							msg.len = sizeof(((struct taddrrep *)&msg)->ip) + sizeof(msg.len) + \
								sizeof(((struct taddrrep *)&msg)->port) + sizeof(msg.msgcode);
							((struct taddrrep *)&msg)->ip = tc->ip_addr;
							((struct taddrrep *)&msg)->port = tc->udpport;

							len = create_tmsg(&tmp, &msg);

							/*
							 * Sending information to requester
							 */
							res = send(tc->waiting->sockfd, tmp, len, 0);
							if (res == -1)
								handle_err("send(): ");
							free(tmp);
							break;

						case AVAILABLE :
							tc = gettclient_bysockfd(clientl, i);
							tc->status = FREE;
							tc->waiting = NULL;

					} //switch (msg.msgcode)

					FD_ZERO(&readset);
					break;
				} //else (not the listener, message from a client)
			} //if (FD_ISSET(i, &readset))
		} //for(...)
	} //while (1)

	res = close(listener);
	if (res == -1)
		handle_err("close(): ");

	return 0;
}
